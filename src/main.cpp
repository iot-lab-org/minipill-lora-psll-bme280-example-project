/**
 * Example and test of OTAA device connection
 * Author: Leo Korbee
 * 2022-10-23
 * Tested on MiniPill LoRa (STM32L051C8T6 with RFM95W)
 * See end of file for connection diagram
 * 
 * Adapted main code with some improvements:
 * - Added DEBUG possibilities
 * - Added security feature/file - do not upload this one to your git-repository!
 * 
 * Adapted library for some improvements:
 * - Added ADR return message for 0x06 Request
 * - Improved DevNonce calculation/randomization
 * - Mac command processing
 * - Confirmed downlink works!
 * - Low Power mode, also during Join and Cycle (send and receive RX1 or RX2)
 * - RTC timing correction for Low Power mode
 * 
 * 2023-05-14
 * - set calTime in secconfig.h and used for calibration.
 * - secconfig should be renamed?
 * 
 * 2023-10-17
 * - added class CLASS_A_NO_RX for devices that only sends data and do not use the 
 *   receive window to reduce power. Updated LoRaMAC.cpp
 * 
 * 2024-06-17
 * - changed secconfig to config.h
 * 
 * 2024-09-29
 * - set framecounter to 32 bits in MIC calculation, framecounter send wil be reset to 0
 * - using unsigned int
 * - remember to set on TTN site to 32 bit for correct MIC check!
 * - testing on TTN04 and TTN08 and TTN12
 * 
 * 2025-01-25
 * - framecountercannot be 0, also at startup and turnover
 * - LoRaWAN.cpp/h
 * - found after join reset to 0 instead to 1, corrected.
 * - testing on TTN14
 * - succesfull changed Framecounter_TX to 32 bit and turnover to 1, and start at 1 at boot
 * 
 * Still to do: 
 * - calibrate with received join message
 * - better seed for random number
 */
//#include <Arduino.h>
#include "lorawan.h"
// include for security credentials
#include "config.h"
#include "STM32LowPowerCal.h"

// include for internal voltage reference
#include "STM32IntRef.h"
// include for BME280
#include "BME280.h"

// uncomment for debugging main
#define DEBUG_MAIN

#ifdef DEBUG_MAIN
  // for debugging redirect to hardware Serial2
  // Tx on PA2
  #define DEBUG_INIT(...) HardwareSerial Serial2(USART2)   // or HardWareSerial Serial2(PA3, PA2);
  #define DEBUG_BEGIN(...) Serial2.begin(9600)
  #define DEBUG_PRINT(...) Serial2.print(__VA_ARGS__)
  #define DEBUG_PRINTLN(...) Serial2.println(__VA_ARGS__)
  DEBUG_INIT();
#else
  #define DEBUG_BEGIN(...)
  #define DEBUG_PRINT(...)
  #define DEBUG_PRINTLN(...)
#endif

/* A BME280 object using SPI, chip select pin PA1 */
BME280 bme(SPI,PA1);

// Sleep this many microseconds. Notice that the sending and waiting for downlink
// will extend the time between send packets. You have to extract this time
#define SLEEP_INTERVAL 300000

char outStr[255];
byte recvStatus = 0;

// see connection diagram at the end of this file
// MOSI pin is used for lowPower settings on RFM module
const sRFM_pins RFM_pins =
{
  .MOSI = PA7,
  .CS = PA4,
  .RST = PA9,
  .DIO0 = PA10,
  .DIO1 = PB4,
  .DIO2 = PB5
//  .DIO5 = ,
};

void setup()
{ 
  // Setup serial debug
  DEBUG_BEGIN();
  // time for serial settings
  delay(1000);
  
 /************************************************
 * get TimeCorrection number for RTC times
 ************************************************/
  // wait 8 seconds to upload new code and
  // calibrate the RTC times with the HSI internal clock
  // time calibration on device with correct timing (ideal 8000.00)
  // incease this time to shorten time between send and receive
  LowPowerCal.setRTCCalibrationTime(calTimeDivider);
  // callibrate for 8 seconds
  LowPowerCal.calibrateRTC();
  DEBUG_PRINT("RTC Time Correction: ");
  DEBUG_PRINTLN(LowPowerCal.getRTCTimeCorrection(), 5);
  
  if(!lora.init())
  {
    DEBUG_PRINTLN("RFM95 not detected");
    delay(5000);
    return;
  }

  // Set LoRaWAN Class change CLASS_A or CLASS_C or CLASS_A_NO_RX
  lora.setDeviceClass(CLASS_A_NO_RX);

  // Set Data Rate for join!
  lora.setDataRate(SF7BW125);

  // Set Receive Delay for RX1 window
  lora.setReceiveDelay1(5);

  // set channel to random
  lora.setChannel(MULTI);

// Join channels:
// 868.10 MHz 868.30 MHz 868.50 MHz -> channel 0, 1, 2


  // use this setting for power settings lora.setTxPower will use boost pin?
  // power level between 0 and 16
  lora.setTxPower1(0);

  // Put OTAA Key and DevAddress here
  lora.setDevEUI(devEui);
  lora.setAppEUI(appEui);
  lora.setAppKey(appKey);
  
  

  // Join procedure
  // https://lora-developers.semtech.com/documentation/tech-papers-and-guides/the-book/joining-and-rejoining
  
  bool isJoined = false;
  do
  {
    DEBUG_PRINTLN("Joining...");
    isJoined = lora.join();
    // faster 
    if (isJoined) break;
    
    //wait for 15s to try again
    lora.sleepLowPower();
    LowPower.begin();
    LowPower.deepSleep(15000);

  }while(!isJoined);
  DEBUG_PRINTLN("Joined to network");

  // set dataRate for sending data instead of defaults
  lora.setDataRate(SF7BW125);

  // begin communication with BME280 and set to default sampling, iirc, and standby settings
  if (bme.begin() < 0)
  {
    DEBUG_PRINTLN("Error communicating with BME280 sensor, please check wiring");
    while(1){}
  }

  // set framecounter for first packet
  // do this after join!
  // default is 1
  // lora.setFrameCounter(65535);

}

void loop()
{
  // Prepare upstream data transmission at the next possible time.
  uint8_t dataLength = 8;
  char data[dataLength];

  // read vcc and add to bytebuffer
  int32_t vcc = IntRef.readVref();
  data[0] = (vcc >> 8) & 0xff;
  data[1] = (vcc & 0xff);

  // reading data from BME sensor
  bme.readSensor();
  float tempFloat = bme.getTemperature_C();
  float humFloat = bme.getHumidity_RH();
  float pressFloat = bme.getPressure_Pa();

  // from float to uint16_t
  uint16_t tempInt = 100 * tempFloat;
  uint16_t humInt = 100 * humFloat;
  // pressure is already given in 100 x mBar = hPa
  uint16_t pressInt = pressFloat/10;

  // move BME data into bytebuffer
  data[2] = (tempInt >> 8) & 0xff;
  data[3] = tempInt & 0xff;

  data[4] = (humInt >> 8) & 0xff;
  data[5] = humInt & 0xff;

  data[6] = (pressInt >> 8) & 0xff;
  data[7] = pressInt & 0xff;

  // set forced mode to be shure it will use minimal power and send it to sleep
  bme.setForcedMode();
  bme.goToSleep();
  

  // unconfirmed data (0), port (1)
  lora.setUplinkData(data, strlen(data), 0, 1);

  // sprintf(myStr, "Hello World! %d", counter);
  // DEBUG_PRINT("Send data: ");
  // DEBUG_PRINTLN(myStr);
  // DEBUG_PRINT("Frame Counter: ");
  // DEBUG_PRINTLN(lora.getFrameCounter());
  // lora.setUplinkData(myStr, strlen(myStr), 0, 1);
  
  // cycle -> send (if new message available) and wait for response in RX1 and RX2
  lora.cycle();

  recvStatus = lora.readData(outStr);
  if(recvStatus)
  {
    DEBUG_PRINTLN(outStr);
    int i = 0;
    while(outStr[i] != '\0' && i < sizeof(outStr))
    {
        DEBUG_PRINT(outStr[i], HEX);
        DEBUG_PRINT(" ");
        i++;
    }
    DEBUG_PRINTLN();
  }

  DEBUG_PRINTLN("Going to long sleep");
  lora.sleepLowPower();
  
  LowPower.begin();
  delay(100);
  // set PA6 to analog to reduce power due to currect flow on DIO to BME280
  // done @ lora.sleepLowPower due to lower power during RX1/RX2 window
  // pinMode(PA6, INPUT_ANALOG);

  LowPower.deepSleep(SLEEP_INTERVAL);
  
}


/* MiniPill LoRa v1.x mapping - LoRa module RFM95W and BME280 sensor
  PA1  //            NSS           - BME280
  PA4  // SPI1_NSS   NSS  - RFM95W
  PA5  // SPI1_SCK   SCK  - RFM95W - BME280
  PA6  // SPI1_MISO  MISO - RFM95W - BME280
  PA7  // SPI1_MOSI  MOSI - RFM95W - BME280

  PA10 // USART1_RX  DIO0 - RFM95W
  PB4  //            DIO1 - RFM95W
  PB5  //            DIO2 - RFM95W

  PA9  // USART1_TX  RST  - RFM95W

  VCC - 3V3
  GND - GND
*/
