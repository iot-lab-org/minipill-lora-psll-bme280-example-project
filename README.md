# PSLL example project with BME280 sensor
With the LMIC library, using OTAA, I could not reduce the power enough to run a my
MiniPill LoRa node on a CR2032 for longer time than a month.

This was irritating because even my ATtiny node performed better than that.
I was very happy with the ATtiny code, because it was straitforward, as LMIC is
not. The build in RTOS is not easy to read and handle to adapt for your
microcontroller.

In this project I use the PSLL library and lowPower options in combination with a
BME280 sensor. This is a Humidity, Pressure and Temperature sensor.

Look at iot-lab.org for information about this project.

## Configuration
Please change the config_example.h to config.h and change credentials and other config info
